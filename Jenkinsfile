#!/usr/bin/env groovy

node {
    stage('Checkout') {
        checkout scm
        def pom = readMavenPom file: 'pom.xml'
        if(pom) {
            echo "Building version ${pom.version}"
        }
        sh "chmod +x ./mvnw"
    }

    stage('Build') {
        withMaven(jdk: 'Java 21', maven: 'Maven 3.9.7') {
            sh "mvn compile"
        }
    }

    stage('Test') {
        withMaven(jdk: 'Java 21', maven: 'Maven 3.9.7') {
            sh "mvn test"
        }
    }

    stage('Quality') {
        withMaven(jdk: 'Java 21', maven: 'Maven 3.9.7'){
            withSonarQubeEnv('SonarCloud') {
                sh 'mvn verify sonar:sonar -Dsonar.projectKey=langeleau_testcicd'
            }
        }
    }

    stage('Package') {
        archiveArtifacts artifacts:'**/target/*.*'
    }

    stage('Generate') {
        withMaven(jdk: 'Java 21', maven: 'Maven 3.9.7'){
            sh 'mvn spring-boot:build-image -Ddocker.host=172.30.176.1:2375 -Dspring-boot.build-image.imageName=registry.gitlab.com/langeleau/testcicd/spring-petclinic:3.0.0-SNAPSHOT'
        }
    }
}